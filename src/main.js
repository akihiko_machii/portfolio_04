// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'

import Vue from 'vue'
import Vue2TouchEvents from 'vue2-touch-events'
import vbclass from 'vue-body-class'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Vuikit from 'vuikit'
import VueAnalytics from 'vue-analytics'
import App from './App'
import Header from './components/parts/_header.vue'
import router from './router'
import Utils from './utils/Utils'

require('waypoints/lib/noframework.waypoints.min.js')

Vue.config.productionTip = false
Vue.use(Vuikit)
Vue.use(vbclass, router)
Vue.use(VueAwesomeSwiper)
Vue.use(VueAnalytics, {
  id: 'UA-7966471-61',
  router
})
Vue.use(Vue2TouchEvents)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App,
    Vuikit
  },
  template: '<App/>'
}, {
  el: '#header',
  components: {
    Header
  },
  template: '<header></header>'
})

/* eslint-disable no-new */
new Utils()
