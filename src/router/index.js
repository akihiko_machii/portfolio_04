import Vue from 'vue'
import Router from 'vue-router'
import Promise from 'bluebird'
import Top from '../components/pages/Top'
import About from '../components/pages/About'
import Interview from '../components/pages/Interview'
import Interview01 from '../components/pages/InterviewDetail01'
import Interview02 from '../components/pages/InterviewDetail02'
import Interview03 from '../components/pages/InterviewDetail03'
import Interview04 from '../components/pages/InterviewDetail04'
import Interview05 from '../components/pages/InterviewDetail05'
import Interview06 from '../components/pages/InterviewDetail06'
import Interview07 from '../components/pages/InterviewDetail07'
import Interview08 from '../components/pages/InterviewDetail08'
import Interview09 from '../components/pages/InterviewDetail09'
import Benefit from '../components/pages/benefit'
import Gallery from '../components/pages/Gallery'
import Entry from '../components/pages/Entry'

export default new Router({
  base: '/freshers/',
  mode: 'hash',
  routes: [{
    path: '/',
    component: Top,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用',
      description: 'エス・グルーヴの新卒採用サイト。ローズバッド、ジル・スチュアート、ラリンなどアパレル、スポーツ、コスメ、カフェなどの接客・販売のプロフェッショナル集団です！会社の紹介やスタッフインタビュー、福利厚生制度などを掲載しています。',
      keywords: 'エス・グルーヴ,S-GROOVE,採用,新卒,リクルート',
      bodyClass: 'top'
    }
  }, {
    path: '/about',
    component: About,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜私たちについて',
      description: 'エス・グルーヴの新卒採用サイト。会社の紹介と教育や研修、ブランドリストを掲載しています。',
      keywords: 'エス・グルーヴ,S-GROOVE,採用,会社概要',
      bodyClass: 'about'
    }
  }, {
    path: '/interview',
    component: Interview,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー',
      description: 'エス・グルーヴの新卒採用サイト。様々な職種のインタビューを掲載しています。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview'
    }
  }, {
    path: '/interview/01',
    component: Interview01,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー01',
      description: 'エス・グルーヴの新卒採用サイト。入社１年目の販売スタッフのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type01'
    }
  }, {
    path: '/interview/02',
    component: Interview02,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー02',
      description: 'エス・グルーヴの新卒採用サイト。入社１年目のエキスパート職のインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type01'
    }
  }, {
    path: '/interview/03',
    component: Interview03,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー03',
      description: 'エス・グルーヴの新卒採用サイト。入社３年目の販売スタッフのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type01'
    }
  }, {
    path: '/interview/04',
    component: Interview04,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー04',
      description: 'エス・グルーヴの新卒採用サイト。入社２年目の販売スタッフのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type01'
    }
  }, {
    path: '/interview/05',
    component: Interview05,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー05',
      description: 'エス・グルーヴの新卒採用サイト。ブランドマネージャーのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type02'
    }
  }, {
    path: '/interview/06',
    component: Interview06,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー06',
      description: 'エス・グルーヴの新卒採用サイト。ショップマネージャーのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type02'
    }
  }, {
    path: '/interview/07',
    component: Interview07,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー07',
      description: 'エス・グルーヴの新卒採用サイト。カフェマネージャーのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type03'
    }
  }, {
    path: '/interview/08',
    component: Interview08,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー08',
      description: 'エス・グルーヴの新卒採用サイト。オフィススタッフのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type03'
    }
  }, {
    path: '/interview/09',
    component: Interview09,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜スタッフインタビュー09',
      description: 'エス・グルーヴの新卒採用サイト。教育・研修コンサルタントのインタビュー。',
      keywords: 'エス・グルーヴ,S-GROOVE,インタビュー',
      bodyClass: 'interview-detail--type03'
    }
  }, {
    path: '/benefit',
    component: Benefit,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜教育・研修・福利厚生',
      description: 'エス・グルーヴの新卒採用サイト。研修や福利厚生について。',
      keywords: 'エス・グルーヴ,S-GROOVE,キャリア,福利厚生',
      bodyClass: 'benefit'
    }
  }, {
    path: '/gallery',
    component: Gallery,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜数字で見るエスグルーヴ',
      description: 'エス・グルーヴの新卒採用サイト。データで見るエス・グルーヴ・ギャラリー',
      keywords: 'エス・グルーヴ,S-GROOVE,データ,ギャラリー',
      bodyClass: 'gallery'
    }
  }, {
    path: '/entry',
    component: Entry,
    meta: {
      title: 'S-GROOVE（エス・グルーヴ）2020新卒採用｜エントリー・募集内容',
      description: 'エス・グルーヴの新卒採用サイト。エントリー内容について。',
      keywords: 'エス・グルーヴ,S-GROOVE,エントリー,募集内容',
      bodyClass: 'entry'
    }
  }],
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 450)
    })
  }
})

Vue.use(Router)
