export default ((u) => {
  if (u.match(/windows nt (\d+(\.\d+)?)/i)) {
    return 'windows'
  } else if (u.match(/iPhone;/i)) {
    return 'iphone'
  } else if (u.match(/iPod;/i)) {
    return 'ipod'
  } else if (u.match(/iPad;/i)) {
    return 'ipad'
  } else if (u.match(/Android (\d+(\.\d+)?)/i)) {
    return 'android'
  }
  return 'others'
})(window.navigator.userAgent.toLowerCase())
