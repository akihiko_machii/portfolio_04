// CheckBrowser

export default (u => {
  if (u.match(/msie (\d+(\.\d+)?)/i) || u.match(/Trident\/(\d+(\.\d+)?)/i)) {
    return 'ie'
  } else if (u.match(/edge\/(\d+(\.\d+)?)/i)) {
    return 'edge'
  } else if (u.match(/chrome\/(\d+(\.\d+)?)/i)) {
    return 'chrome'
  } else if (u.match(/firefox\/(\d+(\.\d+)?)/i)) {
    return 'firefox'
  } else if (u.match(/opera\/(\d+(\.\d+)?)/i)) {
    return 'opera'
  } else if (u.match(/safari\/(\d+(\.\d+)?)/i)) {
    return 'safari'
  }
})(window.navigator.userAgent.toLowerCase())
