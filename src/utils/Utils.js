// Utils.js

import CheckOS from './CheckOS'
import CheckBrowser from './CheckBrowser'
import WindowStore from './WindowStore'

export default class Utils {
  constructor() {
    this.html = document.getElementsByTagName('html')[0]
    this.htmlAddClass()
    this.resize()

    /* eslint-disable no-new */
    new WindowStore()
  }

  htmlAddClass() {
    const addBrowserClassName = `is-${CheckBrowser}`
    const addOSClassName = `is-${CheckOS}`
    this.html.classList.add(addBrowserClassName)
    this.html.classList.add(addOSClassName)
  }

  resize() {
    window.addEventListener('resize', () => {
      new WindowStore()
    })
  }

  loadImage() {
    let img
    const target = document.getElementById('mainVisual')
    let src = this.getBgImage(target)

    if (src !== '') {
      img = new Image()
      img.onload = function() {
        target.classList.add('is-loaded')
      }
      img.src = src
    }
  }

  getBgImage (ele) {
    if (ele) {
      const val = document.defaultView.getComputedStyle(ele, null).getPropertyValue('background-image')
      return val.replace(/(url\(|\)|")/g, '')
    }
  }
}
