import $ from 'jquery'

export default class WindowStore {
  constructor() {
    this.$window = $(window)
    this.html = $('html')
    this.body = $('body')

    this.$window
      .on('load', this.init())
      .on('resize', this.init())
  }

  init() {
    this._windowInfo = {
      width: undefined,
      height: undefined,
      breakP: undefined
    }

    this.setWidthHeight()
    this.setViewport(this._windowInfo.breakP)
  }

  setWidthHeight() {
    this._windowInfo.width = this.$window.width()
    this._windowInfo.height = this.$window.height()

    this.setBreakP(this._windowInfo.width)
  }

  setBreakP(w) {
    if (w <= 768) {
      this._windowInfo.breakP = 'SP'
    } else {
      this._windowInfo.breakP = 'PC'
    }
  }

  setViewport(breakP) {
    let viewportContent
    if (breakP === 'SP') {
      viewportContent = 'width=375,initial-scale=1.0'
    } else {
      viewportContent = 'width=device-width, initial-scale=1.0'
    }
    document.querySelector("meta[name='viewport']").setAttribute('content', viewportContent)
  }
}
